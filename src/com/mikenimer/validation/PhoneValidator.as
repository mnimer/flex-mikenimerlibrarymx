package com.mikenimer.validation 
{
	import mx.validators.RegExpValidator;

	public class PhoneValidator extends RegExpValidator 
	{
		private var regex:String = "^(((1))?[ ,\-,\.]?([\\(]?([1-9][0-9]{2})[\\)]?))?[ ,\-,\.]?([^0-1]){1}([0-9]){2}[ ,\-,\.]?([0-9]){4}(( )((x){0,1}([0-9]){1,5}){0,1})?$";
		
		public function PhoneValidator() 
		{
			this.expression = regex;
			super();
		}
		
	}
}