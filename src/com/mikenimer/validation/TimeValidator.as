package com.mikenimer.validation 
{
	import mx.validators.RegExpValidator;

	public class TimeValidator extends RegExpValidator 
	{
		private var regex:String = "^(([0-1]?[0-9]|[2][1-4]):([0-5]?[0-9])(:[0-5]?[0-9])?).?([AP]M|[AP]m|[ap]m|[ap]M)?$";
		
		public function TimeValidator() 
		{
			this.expression = regex;
			super();
		}
		
	}
}