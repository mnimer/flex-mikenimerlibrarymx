package com.mikenimer.validation 
{
	import mx.validators.RegExpValidator;

	public class EmailValidator extends RegExpValidator 
	{
		private var regex:String = "^[a-zA-Z_0-9-'\+~]+(\.[a-zA-Z_0-9-'\+~]+)*@([a-zA-Z_0-9-]+\.)+[a-zA-Z]{2,7}$";
		
		public function EmailValidator() 
		{
			this.expression = regex;
			super();
		}
		
	}
}