package com.mikenimer.validation 
{
	import mx.validators.RegExpValidator;

	public class SsnValidator extends RegExpValidator 
	{
		private var regex:String = "^[0-9]{3}(-| )[0-9]{2}(-| )[0-9]{4}$";
		
		public function SsnValidator() 
		{
			this.expression = regex;
			super();
		}
		
	}
}