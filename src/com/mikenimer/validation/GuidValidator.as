package com.mikenimer.validation 
{
	import mx.validators.RegExpValidator;

	public class GuidValidator extends RegExpValidator 
	{
		private var regex:String = "[A-Fa-f0-9]{8,8}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{12,12}";
		
		public function GuidValidator() 
		{
			this.expression = regex;
			super();
		}
		
	}
}