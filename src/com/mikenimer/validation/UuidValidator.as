package com.mikenimer.validation 
{
	import mx.validators.RegExpValidator;

	public class UuidValidator extends RegExpValidator 
	{
		private var regex:String = "[A-Fa-f0-9]{8,8}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{4,4}-[A-Fa-f0-9]{16,16}";
		
		public function UuidValidator() 
		{
			this.expression = regex;
			super();
		}
		
	}
}