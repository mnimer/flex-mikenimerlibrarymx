package com.mikenimer.utilities
{
	import com.adobe.utils.StringUtil;

	public class CF
	{

	
	    /**
	     * @return String number that the beginning of a string can be converted to,
	     * or 0 if conversion is not possible
	     * @see #IsNumeric
	     */
	    public static function val(s:String):Number
	    {
	        s = StringUtil.trim(s);
	
			var x:int = 0;
	        var cnt:int=0;
	        for(x = 0; x < s.length; x++)
	        {
	            var c:String = s.charAt(x);
	            //If the input has more than one '.' e.g 65.65.65 etc, ignore input from the second '.'
	            if ( c == '.')
	            {
	                cnt ++;
	            }
	
	            if ((c < '0' || c > '9') && (c != '.' || cnt > 1) && (c != '-' || x > 0) && ( c != '+' || x > 0))
	            {
	                break;
	            }	
	        }
	        try
	        {
	            if (x > 0)
	                return new Number(s.substring(0,x));
	        }
	        catch(err:Error)
	        {
	            // Do nothing, just return 0 if we can't convert to a number for any reason
	        }
	        return 0;
	    }
	    
	    
	}
}