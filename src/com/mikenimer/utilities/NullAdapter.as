package com.mikenimer.utilities
{
	public class NullAdapter
	{
		public static function isNull(object:Object, newValue:Object):Object
		{
			if( object == null)
			{
				return newValue;
			}
			return object;
		}

	}
}