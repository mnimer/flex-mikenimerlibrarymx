package com.mikenimer.components.html.iFrame
{
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	
	import mx.containers.Canvas;


	public class IFrame extends Canvas
	{
		public var displayed:Boolean;
		
		 private var __source: String;
        public var iFrameId:String;

		private var lastX:int;
		private var lastY:int;
		
        /**
         * Move iframe through ExternalInterface.  The location is determined using localToGlobal()
         * on a Point in the Canvas.
         **/
        private function moveIFrame(): void
        {
            var localPt:Point = new Point(0, 0);
            var globalPt:Point = this.localToGlobal(localPt);
            trace("stage: " +stage.x +":" +stage.y +"-- global: " +globalPt.x +":" +globalPt.y);
			if( this.width > 0 && this.height > 0 )
			{
				if( lastX != globalPt.x || lastY != globalPt.y )
				{
					lastX = globalPt.x;
					lastY = globalPt.y;
            		ExternalInterface.call("moveIFrame", iFrameId, globalPt.x, globalPt.y, this.width, this.height);
    			}
   			}
        }


        /**
         * The source URL for the IFrame.  When set, the URL is loaded through ExternalInterface.
         **/
        private var _sourceChanged:Boolean = false;
        public function set source(source: String): void
        {
			__source = source
			_sourceChanged = true;
			invalidateProperties();
        }


        public function get source(): String
        {
            return __source;
        }


        /**
         * Whether the IFrame is visible.  
         **/
		private var _visibleChanged:Boolean = false;
        override public function get visible(): Boolean
        {
        	return super.visible;
        }
        

        override public function set visible(visible: Boolean): void
        {
            super.visible=visible;
			_visibleChanged = true;
			
			if( !visible )
			{
				this.x = -1000;
				this.y = -1000;
				moveIFrame();
			}
			
			invalidateProperties();
			
        }
        

        public function resizeEventHandler(event:Event):void
        {
		    callLater(moveIFrame);
        }

        
        public function moveEventHandler(event:Event):void
        {
		    callLater(moveIFrame);
        }
        
        
        override protected function commitProperties():void
        {
        	if( _visibleChanged )
        	{
        		_visibleChanged = false;
 				if (visible)
	            {
	                //ExternalInterface.call("showIFrame", iFrameId);
	                callLater(moveIFrame);
	            }
	            else 
	            {
	                ExternalInterface.call("hideIFrame", iFrameId);
            	}        		
        	}
        	
        	if( _sourceChanged )
        	{
        		_sourceChanged = false;
        		if (! ExternalInterface.available)
                {
                    throw new Error("ExternalInterface is not available in this container. Internet Explorer ActiveX, Firefox, Mozilla 1.7.5 and greater, or other browsers that support NPRuntime are required.");
                }
                ExternalInterface.call("loadIFrame", iFrameId, source);
                callLater(moveIFrame);
        	}
        }


		public function IFrame()
		{
			super();
			//this.addEventListener("resize", resizeEventHandler);
			//this.addEventListener("move", moveEventHandler);
		}
		
	}
}