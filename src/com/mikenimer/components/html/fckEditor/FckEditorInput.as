package com.mikenimer.components.html.fckEditor
{
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	
	import mx.containers.Canvas;
	import mx.events.FlexEvent;
	import mx.events.MoveEvent;
	import mx.events.ResizeEvent;


	/**
	 * 
	 * Requires the following javascript/html
	 	<script type="text/javascript" src="/fckeditor/fckeditor.js"></script>
	 	<script language="JavaScript" type="text/javascript">
		<!--	
			var fck;
			var lastEdit;
			function fck_createkEditor(divId, w, h, toolbar, value)
			{
				var div = document.getElementById(divId);
				fck = new FCKeditor(divId, w, h, toolbar, value);
				fck.DisplayErrors = true;
				fck.BasePath = "fckeditor/";
				div.innerHTML = fck.CreateHtml();
			}
			
			function FCKeditor_OnComplete( editorInstance )
			{
		    	editorInstance.Events.AttachEvent( 'OnSelectionChange', fck_doSelectionChange ) ;
			}
				
			function fck_setValue(divId, value)
			{
				var oEditor = FCKeditorAPI.GetInstance(divId);
					oEditor.SetHTML(value);
			}
			
			function fck_doSelectionChange( editorInstance )
			{
				//var id = "${application}";
				var html = editorInstance.GetXHTML();
				if( lastEdit != html )
				{
					lastEdit = html;
					${application}.fckSelectionChange(editorInstance.Name, html);
				}
			}
			
			
			function moveDiv(divId,x,y,w,h)
			{
				var ref=document.getElementById(divId);
					ref.style.zIndex=++ref.style.zIndex;
				    ref.style.left=x;
				    ref.style.top=y;   
		    		ref.style.width = w +"px";
					ref.style.height = h +"px";
					fck.Width = w;
					fck.Height = h;
			}
			
			
			function toggleDiv(divId, visible)
			{
				var div = document.getElementById(divId);
				if( visible )
				{
					div.style.visibility="visible";
					ref.style.zIndex=++ref.style.zIndex;
				}else{
					div.style.visibility="hidden";
				}
			}	
		// -->
		</script>
		  
		* one div per instance in an application. 
	 	<div id="fckLetterDiv" style="position:absolute; top:-1px; left:-1px; width:0px; height;0px; background-color:red;z-index:2; visibility:visible;"></div>
		<div id="fckEmailDiv" style="position:absolute; top:-1px; left:-1px; width:0px; height;0px; background-color:red;z-index:3; visibility:visible;"></div>
	 **/

	[Event("change")]
	public class FckEditorInput extends Canvas
	{
		[Bindable]
		public var fckEditorDivId:String = "fckEditorDiv";
		[Bindable]
		public var toolBarSet:String = "Default";
		[Bindable]
		public var value:String = "";
		
		private var _htmlText:String;
		
		[Bindable]
		public function get htmlText():String
		{
			return _htmlText;	
		}
		
		public function set htmlText(s:String):void
		{
			_htmlText = s;
			ExternalInterface.call("fck_setValue", this.fckEditorDivId, s);
		}
		

		/**
		 * Requires the following javscript in the page.
		 * 
		  	function moveDiv(divId,x,y,w,h)
			{
				var div = document.getElementById(divId);
					div.style.left=x;
		    		div.style.top=y;    
		    		div.style.width = w +"px";
					div.style.height = h +"px";
			}
		 **/
		
		private function doCreationComplete(event:Event=null): void
		{
			ExternalInterface.call("fck_createkEditor", this.fckEditorDivId, this.width, this.height, toolBarSet, this.htmlText);
			CLMoveDiv(event);
		} 
		
		
		private function CLMoveDiv(event:Event=null): void
		{
			callLater(moveDiv);
		} 

		
		private function moveDiv(event:Event=null): void 
        {
            var localPt:Point = new Point(0, 0);
            var globalPt:Point = this.localToGlobal(localPt);
            ExternalInterface.call("moveDiv", this.fckEditorDivId, globalPt.x, globalPt.y, this.width, this.height);
        }


		/**
		 * Requires the following javscript in the page.
		 * 
			function toggleDiv(divId, visible)
			{
				var div = document.getElementById(divId);
				if( visible )
				{
					div.style.visibility="visible";
				}else{
					div.style.visibility="hidden";
				}
			}
		 **/
        private function doShow(event:Event=null): void
		{
			ExternalInterface.call("toggleDiv", this.fckEditorDivId, true);
			CLMoveDiv();
		} 
		

        private function doHide(event:Event=null): void
		{
			ExternalInterface.call("toggleDiv", this.fckEditorDivId, false);
			CLMoveDiv();
		} 

        
        override public function set visible(visible: Boolean): void {
            super.visible=visible;
            if (visible)
            {
                ExternalInterface.call("toggleDiv", this.fckEditorDivId, true);
                CLMoveDiv();
            }
            else
            {
                ExternalInterface.call("toggleDiv", this.fckEditorDivId, false);
            }
        }
		
		
		/**
		 * javascript listener. However there is a chance this could be picked up
		 * by all instances. Of this tag in an applcation. So we'll make sure that the 
		 * we only do something if we are the right listener based on the id of the
		 * fckEditor. 
		 **/
		protected function doFckSelectionChange(divId:String, html:String):void
		{
			if( divId == this.fckEditorDivId )
			{
				this.value = html;
				//trace(html);
				dispatchEvent(new Event("change"));
			}
		}
		
		
		/**
		 * requires the following javascript in the page
		 * 		  	
		  	function createFckEditor(divId)
			{
				var div = document.getElementById(divId);
				var fck = new FCKeditor(divId);
				div.innerHTML = fck.CreateHtml();
			}
		 **/
		public function FckEditorInput()
		{
			super();
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE, doCreationComplete);
			this.addEventListener(ResizeEvent.RESIZE, CLMoveDiv);
			this.addEventListener(MoveEvent.MOVE, CLMoveDiv);
			this.addEventListener(FlexEvent.SHOW, doShow);
			this.addEventListener(FlexEvent.HIDE, doHide);
			this.addEventListener(Event.ACTIVATE, doShow);
			
			ExternalInterface.addCallback("fckSelectionChange",doFckSelectionChange);

			
		}		
	}
}