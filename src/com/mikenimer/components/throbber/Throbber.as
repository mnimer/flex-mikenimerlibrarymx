package com.mikenimer.components.throbber
{
	// DDozier: 5-18-2009
	// Per nimer's request, this component was retooled to present the "spokes" design
	// instead of the manner in which the original Throbber class was implemented.
	// 
	// Therefore, because this component has been complelety redone,  I removed the large
	// copyright notice from the original author. I did not change the name to make it 
	// easier to update the application.
	//
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	
	[Style(name="spokeColor", type="uint", format="Color", inherit="no")]
	[Style(name="radiusOuter", type="Number", format="Length", inherit="no")]
	[Style(name="radiusInner", type="Number", format="Length", inherit="no")]
	[Style(name="thickness", type="Number", format="Length", inherit="no")]
	[Style(name="spokeThickness", type="Number", format="Length", inherit="no")]
	[Style(name="maxAlpha", type="Number", format="Color", inherit="no")]
	[Style(name="minAlpha", type="Number", format="Color", inherit="no")]
	[Style(name="msecTimer", type="uint", format="Length", inherit="no")]

	public class Throbber extends UIComponent
	{
		public var color:uint			= 0x4F4F4F;
		public var radOuter:Number		= 15.0;
		public var radInner:Number		= 5.0;
		public var thickness:Number		= 1.5;
		public var maxAlpha:Number		= 1.0;
		public var minAlpha:Number		= 0.05;
		public var msecTimer:uint		= 80;

		private var degPerTick:uint		= 360 / _spokes;
		private var timer:Timer			= null;
		private var sprite:Sprite		= null;

		private var _spokes:uint		= 12;
		private var _autoStart:Boolean	= true;
		private var didRetrieveStyles:Boolean	= false;

        public var hasBeenDrawn:Boolean = false;


		//---------------------------------------
		//
		public function Throbber()
		{
		}

		//---------------------------------------
		//
		private function retrieveStyles():void
		{
			if (didRetrieveStyles)
			{
				return;
			}

			color		= getStyle("spokeColor") as uint;
			radOuter	= getStyle("radiusOuter") as Number;
			radInner	= getStyle("radiusInner") as Number;
			thickness	= getStyle("spokeThickness") as Number;
			maxAlpha	= getStyle("maxAlpha") as Number;
			minAlpha	= getStyle("minAlpha") as Number;
			msecTimer	= getStyle("msecTimer") as uint;

			didRetrieveStyles = true;
		}

		//---------------------------------------
		//
		private function draw():void
		{
			retrieveStyles();

			//trace("Throbber radOuter=" + radOuter.toString());
            this.addEventListener(FlexEvent.SHOW, showHandler);
            this.addEventListener(FlexEvent.ADD, showHandler);
            this.addEventListener(FlexEvent.HIDE, hideHandler);
            this.addEventListener(FlexEvent.REMOVE, hideHandler);

			if (!sprite)
			{
				sprite = new Sprite();
			}

			// If we made it to this point, properties have been changed; clear the instance
			// and re-render.
			//
			var gr:Graphics = sprite.graphics;
			gr.clear();

			// The strategy is to take advantage of the rotation property of the Sprite. We
			// render the single view of the component here, then rotate the complete image
			// during the timer tick handler.
			//
			var xCtr:Number	= radOuter;
			var yCtr:Number	= radOuter;

			sprite.x	= xCtr;
			sprite.y	= yCtr;
			addChild(sprite);

			var lenSpoke:Number		= radOuter - radInner;			
			var pntInner:Point		= new Point();
			var pntOuter:Point		= new Point();
			var piDiv180:Number		= Math.PI/180
			var angle:Number		= 0;
			var alpha:Number		= maxAlpha;
			var alphaStep:Number	= (maxAlpha - minAlpha) / _spokes;

			var rads:Number;
			var sin:Number;
			var cos:Number;

			for (var iSpoke:uint = 0; iSpoke < _spokes; ++iSpoke)
			{
				// Set up the current line style.
				gr.lineStyle(thickness, color, alpha, true, "none", "none", "miter", 1);
				
				rads	= angle * piDiv180;
				sin		= Math.sin(rads);
				cos		= Math.cos(rads);

				pntInner.x	= radInner * sin; 
				pntInner.y	= radInner * cos; 

				pntOuter.x	= radOuter * sin; 
				pntOuter.y	= radOuter * cos; 

				gr.moveTo(pntInner.x, pntInner.y);
				gr.lineTo(pntOuter.x, pntOuter.y);

				angle	+= degPerTick;
				alpha	-= alphaStep;
			}

			gr.endFill();

			// I expected 0 degrees to be the 12:00 position, but it ends up
			// at the 6:00 position. So, I adjust it here.
			// 
			sprite.rotation = 180;

            hasBeenDrawn = true;
		}

		//---------------------------------------
		//
		public function get spokes():uint
		{
			return _spokes;
		}

		public function set spokes(value:uint):void
		{
			_spokes		= value;
			degPerTick	= 360 / _spokes;
		}

		//---------------------------------------
		//
		public function get autoStart():Boolean
		{
			return _autoStart;
		}

		public function set autoStart(value:Boolean):void
		{
			//trace("autoStart=" + value.toString());
			_autoStart = value;
		}

		//---------------------------------------
		//
		public function get isPlaying():Boolean
		{
			return timer ? true: false;;
		}

		//---------------------------------------
		//
		override public function get measuredHeight():Number
		{
			return 2 * radOuter;
		}

		//---------------------------------------
		//
		override public function get measuredWidth():Number
		{
			return 2 * radOuter;
		}

		//---------------------------------------
		//
        public function showHandler(event:Event):void
        {
            play();
        }

        public function hideHandler(event:Event):void
        {
            stop();
        }

		//---------------------------------------
		//
		public function play():void
		{
			stop();
			if(sprite)
            {
                sprite.rotation = 180;
            }
            else
            {
                draw();      
            }

			timer = new Timer(msecTimer, 50);
			timer.addEventListener(TimerEvent.TIMER, tick, false, 0, true);
			timer.start();

		}

		//---------------------------------------
		//
		public function stop():void
		{
			if (timer)
			{
				timer.stop();
				timer.removeEventListener(TimerEvent.TIMER, tick);
				timer = null;
			}
		}

		//---------------------------------------
		//
		private function tick(ev:TimerEvent=null):void
		{
            if(sprite)
            {
                //trace(sprite.rotation);
			    sprite.rotation += degPerTick;
            }
            //callLater(tick);
		}

		//---------------------------------------
		//
		override protected function commitProperties():void
		{
			//trace("commitProperties");

			//stop();

            if( !hasBeenDrawn )draw();

			if (_autoStart)
			{
				play();
			}
		}
	}
}
