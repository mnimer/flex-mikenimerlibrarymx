package com.mikenimer.components.controls.typeAheadTextInput
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import mx.collections.ListCollectionView;
	import mx.collections.Sort;
	import mx.controls.Menu;
	import mx.controls.TextInput;
	import mx.events.MenuEvent;

	public class TypeAheadTextInput extends TextInput
	{
		public function TypeAheadTextInput()
		{
			super();
			this.addEventListener(Event.CHANGE, changeEventHandler);
			this.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);

			dataProviderSort = new Sort();
			dataProviderSort.compareFunction = dataProviderCompare;			
		}
		
		private var menu:Menu;
		private var dataProviderSort:Sort;
		
		private var _dataProvider:ListCollectionView;
		private var _dataProviderChanged:Boolean = false;
		[Bindable(event="dataProviderChanged")]
		public function get dataProvider():ListCollectionView
		{
			return _dataProvider;
		}
		public function set dataProvider(list:ListCollectionView):void
		{
			_dataProvider = list;
			_dataProviderChanged = true;
			invalidateProperties();
			dispatchEvent(new Event("dataProviderChanged"));
		}
		
		
		private var _labelField:String = null;
		private var _labelFieldChanged:Boolean = false;
		[Bindable(event="labelFieldChanged")]
		public function get labelField():String
		{
			return _labelField;
		}
		public function set labelField(label:String):void
		{
			_labelField = label;
			_labelFieldChanged = true;
			invalidateProperties();
			dispatchEvent(new Event("labelFieldChanged"));
		}
		
		
		private function dataProviderFilter(item:Object):Boolean
		{
			if( labelField != null )
			{
				return item[labelField].toString().substring(0,this.text.length).toLowerCase() == this.text.toLowerCase();
			}else{
				return item.toString().substring(0,this.text.length).toLowerCase() == this.text.toLowerCase();
			}	
		}
		
		
		private function dataProviderCompare(o1:Object, o2:Object, fields_:Array=null):int
		{
			if( labelField != null )
			{
				if( o1[labelField] < o2[labelField] ) return -1;	
				else if( o1[labelField] > o2[labelField] ) return 1;
				return 0;
			}else
			{
				if( o1 < o2 ) return -1;	
				else if( o1 > o2 ) return 1;
				return 0;
			}	
		}
		
		override protected function keyUpHandler(event:KeyboardEvent):void
		{
			super.keyUpHandler(event);
			if( event.keyCode == Keyboard.DOWN )
			{
				if( this.menu == null )
				{
					buildmenu();
				}
				this.menu.setFocus();
			}
			
		} 
		
		
		private function itemClickHandler(event:MenuEvent):void
		{
			this.text = event.item.toString();
			this.textField.setSelection(this.text.length, this.text.length);
			this.textField.setFocus();
			this.menu.hide();
			
		}
		
		
		private function changeEventHandler(event:Event):void
		{
			if( dataProvider )
			{
				dataProvider.filterFunction = dataProviderFilter;
				dataProvider.sort = dataProviderSort;
				dataProvider.refresh();	
			
				if( dataProvider.length > 0 )
				{
					buildmenu();
					this.textField.setFocus();
					this.textField.setSelection(this.text.length, this.text.length);			
				}
			}
		}
		
		
		private function buildmenu():void
		{
			this.menu = Menu.createMenu(this.parent, dataProvider, true);
			this.menu.labelField = this.labelField;
			this.menu.addEventListener(MenuEvent.ITEM_CLICK, itemClickHandler, false, 0, true);
			
			var pt:Point = new Point( this.x, this.y );
			var globalPt:Point = localToGlobal(pt);
			this.menu.show( globalPt.x - (this.width/2), globalPt.y+this.height);
			
			//this.setFocus();
		}
		
	}
}