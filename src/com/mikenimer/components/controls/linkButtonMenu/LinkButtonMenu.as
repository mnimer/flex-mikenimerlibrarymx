package com.mikenimer.components.controls.linkButtonMenu
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ListCollectionView;
	import mx.controls.LinkButton;
	import mx.controls.Menu;
	import mx.controls.Text;
	import mx.events.MenuEvent;

	[Event(name="itemClick", type="mx.events.MenuEvent")]
	public class LinkButtonMenu extends LinkButton
	{
		public function LinkButtonMenu()
		{
			super();
			this.addEventListener(MouseEvent.CLICK, menuClickHandler, false, 0, true);
		}
		
		private var menu:Menu;
		private var arrow:Text;
		[Bindable]
		public var menuLabelField:String;
		[Bindable]
		public var menuDataField:String;
		
		private var _menuItems:ListCollectionView
		private var _menuItemsChanged:Boolean = false;
		[Bindable(event="menuItemsChanged")]
		public function get menuItems():ListCollectionView
		{
			return _menuItems;
		}
		public function set menuItems(menuItems_:ListCollectionView):void
		{
			_menuItems = menuItems_;
			_menuItemsChanged = true;
			invalidateProperties();			
			dispatchEvent( new Event("menuItemsChanged"));
		}
		
		
		protected function menuClickHandler(event:MouseEvent):void
		{
			var pt:Point = new Point( event.localX, event.localY );
			var globalPt:Point = localToGlobal(pt);
			this.menu.show( globalPt.x - (event.localX), globalPt.y +(this.height-event.localY));
		}		
		
		protected function itemClickHandler(event:MenuEvent):void
		{
			dispatchEvent(event.clone());
		}
		
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if( _menuItemsChanged )
			{
				_menuItemsChanged = false;
				
				this.menu = Menu.createMenu(this, menuItems, true);
				if( this.menuLabelField != null )
				{
					this.menu.labelField = this.menuLabelField;
				}
				this.menu.addEventListener(MenuEvent.ITEM_CLICK, itemClickHandler, false, 0, true);		
			}
		}
		
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			/* 
			if( arrow && !this.contains(arrow) )
			{
				this.addChildAt(arrow, this.numChildren);
			} */	
		}
		
		
		override protected function createChildren():void
		{
			super.createChildren();
			//arrow = new Text();
			//arrow.text = "abcdefghijklmnopqrstuvwxyz";
			//arrow.setStyle("fontFamily", "wingdings");
			//arrow.setStyle("fontSize", "10");	
		}
	}
}