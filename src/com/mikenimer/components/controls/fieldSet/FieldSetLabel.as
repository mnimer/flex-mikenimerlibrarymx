/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * 
 **/
package com.mikenimer.components.controls.fieldSet
{
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.skins.Border;
	import mx.skins.RectangularBorder;
	import mx.utils.ColorUtil;
	import com.mikenimer.components.controls.fieldSet.borders.FieldSetLabelBorder;
	import mx.containers.Box;
	import mx.binding.utils.ChangeWatcher;
	
	
	
	// style default come from the Flex Framework "../styles/metadata/TextStyles.as" file
	[Style(name="color", type="uint", format="Color", inherit="yes")]
	[Style(name="disabledColor", type="uint", format="Color", inherit="yes")]
	[Style(name="fontAntiAliasType", type="String", enumeration="normal,advanced", inherit="yes")]
	[Style(name="fontFamily", type="String", inherit="yes")]
	[Style(name="fontGridFitType", type="String", enumeration="none,pixel,subpixel", inherit="yes")]
	[Style(name="fontSharpness", type="Number", inherit="yes")]
	[Style(name="fontSize", type="Number", format="Length", inherit="yes")]
	[Style(name="fontStyle", type="String", enumeration="normal,italic", inherit="yes")]
	[Style(name="fontThickness", type="Number", inherit="yes")]
	[Style(name="fontWeight", type="String", enumeration="normal,bold", inherit="yes")]
	[Style(name="kerning", type="Boolean", inherit="yes")]
	[Style(name="letterSpacing", type="Number", inherit="yes")]
	[Style(name="textAlign", type="String", enumeration="left,center,right", inherit="yes")]
	[Style(name="textIndent", type="Number", format="Length", inherit="yes")]
	

	public class FieldSetLabel extends Box
	{
		public var legend:Label;
		
		public function FieldSetLabel()
		{
			super();
			this.setStyle("borderStyle", "solid");
			this.setStyle("borderSkin", FieldSetLabelBorder);
		}
		
		override public function get label():String
		{
			return super.label;
		}
		
		override public function set label(l:String):void
		{
			super.label = l;
			if( this.legend != null )
			{
				this.legend.text = l;
			}
		}
		
		public function getLabelWidth():int
		{
			return this.legend.textWidth;
		}
		
		override protected function createChildren():void
		{						
			super.createChildren();
			
			var cr:int = getStyle("cornerRadius");
			legend = new Label();
			legend.text = this.label;
			legend.setStyle("paddingLeft", cr);
			
			legend.setStyle("color", this.getStyle("color")); 
			legend.setStyle("kerning", this.getStyle("kerning")); 
			legend.setStyle("letterSpacing", this.getStyle("letterSpacing")); 
			legend.setStyle("disabledColor", this.getStyle("disabledColor")); 
			legend.setStyle("fontAntiAliasType", this.getStyle("fontAntiAliasType")); 
			legend.setStyle("fontFamily", this.getStyle("fontFamily")); 
			legend.setStyle("fontSize", this.getStyle("fontSize")); 
			legend.setStyle("fontStyle", this.getStyle("fontStyle")); 
			legend.setStyle("fontThickness", this.getStyle("fontThickness")); 
			legend.setStyle("fontSharpness", this.getStyle("fontSharpness")); 
			legend.setStyle("textAlign", this.getStyle("textAlign")); 
			legend.setStyle("textDecoration", this.getStyle("textDecoration"));
			legend.setStyle("textIndent", this.getStyle("textIndent"));  
			
			
			this.addChildAt(legend, 0);
			
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);	
		}
		
	}
}