/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * 
 **/
package com.mikenimer.components.controls.fieldSet.borders
{
	import mx.skins.RectangularBorder;
	import mx.core.EdgeMetrics;
	import flash.display.Graphics;
	import mx.controls.Label;
	import com.mikenimer.components.controls.fieldSet.FieldSet;

	public class FieldSetBorder extends RectangularBorder
	{
		public function FieldSetBorder()
		{
			super();
		}
		
		//----------------------------------
		//  borderMetrics
		//----------------------------------
	
		/**
		 *  @private
		 *  Storage for the borderMetrics property.
		 */
		private var _borderMetrics:EdgeMetrics;
	
		/**
		 *  @private
		 */
		override public function get borderMetrics():EdgeMetrics
		{		
			if (_borderMetrics)
				return _borderMetrics;
				
			var borderStyle:String = getStyle("borderStyle");
		
			_borderMetrics = new EdgeMetrics(3, 1, 3, 3);
	 		
			return _borderMetrics;
		}		
			
		/**
		 *  @private
		 *  Draw the background and border.
		 */
		override protected function updateDisplayList(w:Number, h:Number):void
		{	
			super.updateDisplayList(w, h);
	
			var borderAlpha:int = 1.0;
			var borderThickness:int = getStyle("borderThickness");
			var borderStyle:String = getStyle("borderStyle");
			var borderColor:uint = getStyle("borderColor");
			var cornerRadius:Number = getStyle("cornerRadius");
			var backgroundColor:uint = getStyle("backgroundColor");
			var backgroundAlpha:Number= getStyle("backgroundAlpha");
			
			
			var labelWidth:int = 0;
			var labelHeight:int = 20;
			if( this.parent is FieldSet )
			{
				labelWidth = FieldSet(this.parent).getLabelWidth();
				labelHeight = FieldSet(this.parent).legend.height;
			} 
	
	
			var g:Graphics = graphics;
			g.clear();
			
			g.lineStyle( borderThickness, borderColor, borderAlpha );							
		    
		    var startX:Number = labelWidth +cornerRadius + 10;
		    var startY:Number = 10;//Math.round(labelHeight/2);;

			if( backgroundColor != 0 )
				g.beginFill(backgroundColor, backgroundAlpha);
				
			g.moveTo( startX, startY );
			g.lineTo( width-cornerRadius, startY );
			g.curveTo(width, startY, width, startY+cornerRadius);	
			g.lineTo( width, height-cornerRadius );	
			g.curveTo(width, height, width-cornerRadius, height);	
			g.lineTo( 0+cornerRadius, height );	
			g.curveTo(0, height, 0, height-cornerRadius);	
			g.lineTo( 0, startY+cornerRadius );	
			g.curveTo(0, startY, cornerRadius, startY);	
			g.lineTo( cornerRadius+0, startY );
			
			if( backgroundColor != 0 )
				g.endFill();
			
			/*
			// border 
			drawRoundRect(
				0, 10, w, h, 3,
				borderColor, backgroundAlpha); 
	
			// top pointer 
			g.beginFill(borderColor, backgroundAlpha);
			g.moveTo(9, 11);
			g.lineTo(15, 0);
			g.lineTo(21, 11);
			g.moveTo(10, 11);
			g.endFill();
			*/
		}
				
	}
}