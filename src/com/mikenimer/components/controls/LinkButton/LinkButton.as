package com.mikenimer.components.controls.LinkButton
{
	import mx.controls.LinkButton;
	import mx.core.mx_internal;

	use namespace mx_internal;
	public class LinkButton extends mx.controls.LinkButton
	{
		public function LinkButton()
		{
			super();
		}
		
		public function get extraSpacing():Number
		{
			return mx_internal::extraSpacing;
		}
		
		public function set extraSpacing(n:Number):void
		{
			mx_internal::extraSpacing = n;
		}
	}
}