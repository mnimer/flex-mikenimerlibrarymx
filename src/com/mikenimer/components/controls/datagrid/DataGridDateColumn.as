package com.mikenimer.components.controls.datagrid
{
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.mx_internal;
	import mx.formatters.DateFormatter;
	
	use namespace mx_internal;
	
	public class DataGridDateColumn extends DataGridColumn
	{
		public function DataGridDateColumn(columnName:String=null)
		{
			super(columnName);
			dateFormat = "MM/DD/YYYY";
		}
		
		public static var dateFormatter:DateFormatter = new DateFormatter();
		
		
		public function get dateFormat():String
		{
			return DataGridDateColumn.dateFormatter.formatString;
		}
		
		public function set dateFormat(s:String):void
		{
			DataGridDateColumn.dateFormatter.formatString = s;
		}
		
		private var _labelFunction:Function;
		[Bindable]
		override public function get labelFunction():Function
	    {
	    	if( _labelFunction == null )
	    	{
	    		return dateLabelFunction;
	    	}
	        return _labelFunction;
	    }
	    
	    public function set labelFunction(value:Function):void
	    {
	        _labelFunction = value;
	
	        if (mx_internal::owner)
	        {
	            mx_internal::owner.invalidateList();
	        }
	
	        dispatchEvent(new Event("labelFunctionChanged"));
	    }
	    
	    
	    private var _sortCompareFunction:Function;
	    [Bindable]
	    override public function get sortCompareFunction():Function
	    {
	    	if( _sortCompareFunction == null )
	    	{
	    		return dateLabelSortFunction;
	    	}
	        return _sortCompareFunction;
	    }
	    
	    override public function set sortCompareFunction(value:Function):void
	    {
	        _sortCompareFunction = value;
	
	        dispatchEvent(new Event("sortCompareFunctionChanged"));
	    }	    
	    
	    
	    private function dateLabelFunction(item:Object, col:DataGridColumn):String
	    {
	    	return DataGridDateColumn.dateFormatter.format(item[col.dataField]);
	    }
	    
	    
	    private function dateLabelSortFunction(item1:Object, item2:Object):int
	    {
	    	if( item1[dataField]!=null && item2[dataField]!=null && (item1[dataField] as Date).getTime() < (item2[dataField] as Date).getTime() ) return -1;
	    	if( item1[dataField]!=null && item2[dataField]!=null && (item1[dataField] as Date).getTime() > (item2[dataField] as Date).getTime() ) return 1;
	    	return 0;
	    }
		
	}
}