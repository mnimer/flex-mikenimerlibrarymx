package com.mikenimer.components.controls.datagrid
{
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.mx_internal;
	
	use namespace mx_internal;
	
	
	/**
	 *	Build a DataGrid that support a dot notation path to the sub object property for the dataField
	 * 
	 * WARNING: NON-FUNCTION (this is a work in progress and does not work yet)
	 **/
	public class DataGridObjectColumn extends DataGridColumn
	{
		public function DataGridObjectColumn(columnName:String=null)
		{
			super(columnName);
		}
		
		
		private var _labelFunction:Function;
		[Bindable]
		override public function get labelFunction():Function
	    {
	    	if( _labelFunction == null )
	    	{
	    		return objectLabelFunction;
	    	}
	        return _labelFunction;
	    }
	    
	    public function set labelFunction(value:Function):void
	    {
	        _labelFunction = value;
	
	        if (mx_internal::owner)
	        {
	            mx_internal::owner.invalidateList();
	        }
	
	        dispatchEvent(new Event("labelFunctionChanged"));
	    }
	    
	    /*
	    private var _sortCompareFunction:Function;
	    [Bindable]
	    override public function get sortCompareFunction():Function
	    {
	    	if( _sortCompareFunction == null )
	    	{
	    		return objectLabelSortFunction;
	    	}
	        return _sortCompareFunction;
	    }
	    
	    override public function set sortCompareFunction(value:Function):void
	    {
	        _sortCompareFunction = value;
	
	        dispatchEvent(new Event("sortCompareFunctionChanged"));
	    }	    
	    */
	    
	    private function objectLabelFunction(item:Object, col:DataGridColumn):String
	    {
	    	if( col.dataField.indexOf(".") )
	    	{
	    		var keys:Array = col.dataField.split(".");
	    		var result:Object = item;
	    		for( var i:int=0; i < keys.length; i++ )
	    		{
	    			result = result[keys[i]];
	    		}	
	    		return result.toString();
	    	}
	    	return item[col.dataField];
	    }
	    
	    /*
	    private function objectLabelSortFunction(item1:Object, item2:Object):int
	    {
    		var result1:Object = item1;
    		var result2:Object = item2;
    		
	    	if( this.dataField.indexOf(".") )
	    	{
	    		var keys:Array = this.dataField.split(".");
	    		for( var i:int=0; i < keys.length; i++ )
	    		{
	    			result1 = result1[keys[i]];
	    			result2 = result2[keys[i]];
	    		}	
	    	}
	    	
	    	if( result1 < result2 ) return -1;
	    	if( result1 > result2 ) return 1;
	    	return 0;
	    }
		*/
	}
}