package com.mikenimer.components.controls.TextInputMask 
{
	import com.mikenimer.formatters.MaskFormatter;
	
	import flash.events.Event;
	
	import mx.controls.TextInput;

	public class TextInputMask extends TextInput 
	{
		private var _mask:String;
		private var maskFormatter:MaskFormatter;
		
		public function TextInputMask() 
		{
			super();
			this.addEventListener("change", onChange);
			this.addEventListener("creationComplete", onChange);
		}
		
		public function set inputMask(mask:String):void
		{
			this._mask = mask;
			this.maskFormatter = new MaskFormatter(this, this._mask);			
		}
		public function get inputMask():String
		{
			return this._mask;	
		}
		
		private function onChange(event:Event):void
		{
			if( this.maskFormatter != null )
			{
				this.text = this.maskFormatter.format(event.currentTarget.text);		
				this.validateNow();
				this.setSelection(this.text.length, this.text.length);
			}
			//Alert.show( ObjectUtil.toString(event.currentTarget.text)) ;
		}
		
	}
}