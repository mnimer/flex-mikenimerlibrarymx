/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * A generic Tree Node with Children
 */
package com.mikenimer.components.debug.model
{
	import mx.collections.ArrayCollection;
	
	public class TreeParent extends TreeObject
	{
	    [Bindable]
	    public var children:ArrayCollection;

		public function TreeParent(id:String, value:Object)
		{
			super(id, value);
	        // if the reference item is an array add the items as children
	        this.children = parseArrayChildren(value);			
		}
		
		protected function parseArrayChildren(obj:Object):ArrayCollection
	    {
			if( obj is Array && (obj as Array).length > 0 )
			{
				var items:Array = obj as Array;
				this.children = new ArrayCollection();					
				for( var i:int=0; i < items.length; i++)
				{
					var debugObject:TreeParent = new TreeParent("[" +i +"]", items[i]);
					this.children.addItem(debugObject);	
				}					
				return this.children;
			}
			return null;
	    }
	    	
	    public function isToggled(item:Object):Boolean
	    {
	    	return true;
	    }	
		
	}
}