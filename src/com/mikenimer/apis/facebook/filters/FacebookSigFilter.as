package com.mikenimer.apis.facebook.filters
{
	import com.adobe.crypto.MD5;
	import com.mikenimer.framework.filters.AbstractAsyncFilter;
	import com.mikenimer.framework.filters.IAsyncFilter;
	
	import flash.utils.ByteArray;
	
	import mx.rpc.AsyncToken;
	
	public class FacebookSigFilter extends AbstractAsyncFilter
	{
		private var token:AsyncToken;
		private var method:String;
		private var args:Object;
		
		public function FacebookSigFilter(method:String, args:Object=null, nextFilter:IAsyncFilter=null)
		{
			super(nextFilter);
			this.method = method;
			this.args = args;
		}
		
		
		override public function invoke():AsyncToken
		{
			generateSig(args);
			
			
			//trace("{FacebookSigFilter}.invoke()");
			token = this.next.invoke();
			token.addResponder(this);
			return token;
		}
		
		override public function result(event:Object):void
		{
			//
		}
		
		override public function fault(event:Object):void
		{
			//
		}


		private function generateSig(args:Object):void
		{
			if( args.secret != null )
			{
				var secret:String = args.secret;
				 
				var sortedArray:Array = [];
                        
                for( var key:String in args )
                {
	                var arg:* = args[key];
	                if( key !== 'sig' &&  key !== 'secret' )
	                {
	                	if( !(arg is ByteArray) )
	                	{
                        	sortedArray.push( key + '=' + arg.toString() );
                  		}
	                }
	                else
	                {
	                	delete args[key];	
	                }
                }
                
                sortedArray.sort();
                
                var s:String = '';                
                for( var i:Number = 0; i < sortedArray.length; i++ )
                {
                    s += sortedArray[i];
                }                
                s += secret;
                
                args['sig'] = MD5.hash( s );
			}
		}
	}
}