package com.mikenimer.apis.facebook.commands.auth
{
	import com.mikenimer.apis.facebook.FacebookServiceFactory;
	import com.mikenimer.framework.command.CommandAdapter;
	
	import mx.core.Application;

	public class FacebookAuthGetTokenCommand extends CommandAdapter
	{
		public var apiKey:String;
		public var secret:String;
		public var version:String = "1.0";
		
		public function FacebookAuthGetTokenCommand(apiKey_:String, secret_:String, v_:String="1.0")
		{
			super();
			this.apiKey = apiKey_;
			this.secret = secret_;
			this.version = v_;
		}
		
		override public function execute():void
		{
			FacebookServiceFactory.getInstance().getAuthService(this).auth_createToken(this.apiKey, this.secret, this.version);
		}
		
	}
}