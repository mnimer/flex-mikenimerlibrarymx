package com.mikenimer.apis.facebook.commands.auth
{
	import com.mikenimer.apis.facebook.FacebookServiceFactory;
	import com.mikenimer.framework.command.CommandAdapter;

	public class FacebookAuthGetSessionCommand extends CommandAdapter
	{
		public var apiKey:String;
		public var secret:String;
		public var version:String = "1.0";
		public var authToken:String;
		
		public function FacebookAuthGetSessionCommand(apiKey_:String, secret_:String, authToken_:String, v_:String="1.0")
		{
			super();
			this.apiKey = apiKey_;
			this.secret = secret_;
			this.version = v_;
			this.authToken = authToken_;
		}
		
		override public function execute():void
		{
			FacebookServiceFactory.getInstance().getAuthService(this).auth_getSession(this.apiKey, this.secret, this.authToken, this.version);
		}
	}
}