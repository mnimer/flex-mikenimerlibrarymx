package com.mikenimer.apis.facebook.remote
{
	import com.mikenimer.apis.facebook.IFacebookAuthService;
	import com.mikenimer.apis.facebook.filters.FacebookSigFilter;
	import com.mikenimer.framework.filters.HttpServiceInvokeFilter;
	import com.mikenimer.framework.filters.IAsyncFilter;
	import com.mikenimer.framework.services.ServiceFilterAdapter;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	public class FacebookAuthServiceRemote extends ServiceFilterAdapter implements IFacebookAuthService
	{
		
		public function FacebookAuthServiceRemote(responder:IResponder, service:*)
		{
			super(responder, service);
		}
		
		
		override public function getFilterChain(method_:String, args_:Object):IAsyncFilter
		{
			var filter:IAsyncFilter = new HttpServiceInvokeFilter(this.service, method_, args_);
				filter = new FacebookSigFilter(method_, args_, filter);
			//	filter = new ErrorFilter(filter);
			return filter;
		}
		
		
		public function auth_createToken(apiKey_:String, secret_:String, v_:String="1.0"):AsyncToken
		{
			//
			var args:Object = {method:"auth.createToken", api_key:apiKey_, secret:secret_, v:v_};
			return super.invoke("auth.createToken", args);
		}
		
		
		/**
		 * authToken_ = the token returned by auth_createToken();
		 **/
		public function auth_getSession(apiKey_:String, secret_:String, authToken_:String, v_:String="1.0"):AsyncToken
		{
			//
			var args:Object = {method:"auth.getSession", api_key:apiKey_, secret:secret_, v:v_, auth_token:authToken_};
			return super.invoke("auth.getSession", args);
		}
		

	}
}