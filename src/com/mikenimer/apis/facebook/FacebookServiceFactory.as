package com.mikenimer.apis.facebook
{
	import com.mikenimer.apis.facebook.remote.FacebookAuthServiceRemote;
	
	import mx.rpc.IResponder;
	import mx.rpc.http.mxml.HTTPService;
	
	public class FacebookServiceFactory
	{
		private static var _factory:FacebookServiceFactory;
		private var _authService:HTTPService
		
		public static function getInstance():FacebookServiceFactory
		{
			if( _factory == null )
			{
				_factory = new FacebookServiceFactory();
			}
			return _factory;
		}
		
		
		public function getAuthService(resp_:IResponder):IFacebookAuthService
		{
			if( _authService == null )
			{
				_authService = new HTTPService();
				_authService.method = "GET";
				_authService.url = "http://api.facebook.com/restserver.php";
				_authService.resultFormat = "e4x";
				
				HTTPService(_authService).showBusyCursor = true;
			}
			return new FacebookAuthServiceRemote(resp_, _authService);	
		}

	}
}