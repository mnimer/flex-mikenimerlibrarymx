package com.mikenimer.apis.facebook
{
	import mx.rpc.AsyncToken;
	
	public interface IFacebookAuthService
	{
		function auth_createToken(apiKey_:String, secret_:String, v_:String="1.0"):AsyncToken
		
		/**
		 * authToken_ = the token returned by auth_createToken();
		 **/
		function auth_getSession(apiKey_:String, secret_:String, authToken_:String, v_:String="1.0"):AsyncToken
		
		
	}
}