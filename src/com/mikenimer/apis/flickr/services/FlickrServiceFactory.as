package com.mikenimer.apis.flickr.services
{
	import com.mikenimer.apis.flickr.services.stub.PhotoService;
	import com.mikenimer.apis.flickr.services.xmlrpc.PhotoServiceXmlRpc;
	
	import flash.utils.Endian;
	
	import mx.core.Application;
	import mx.messaging.*;
	import mx.messaging.channels.AMFChannel;
	import mx.messaging.channels.HTTPChannel;
	import mx.rpc.AbstractService;
	import mx.rpc.IResponder;
	import mx.rpc.http.mxml.HTTPService;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.rpc.soap.mxml.WebService;
	import com.mikenimer.apis.flickr.services.rest.PhotoServiceRest;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	
	
	public class FlickrServiceFactory
	{
		private static var _isUseRemoteServices:Boolean = true;
		private static var _useRest:Boolean = true;
		// cached services
		private static var _serviceFactory:FlickrServiceFactory;
		private static var _photoServiceProxy:HTTPService;
						
		public static function getInstance():FlickrServiceFactory {
			if (_serviceFactory == null) {
				_serviceFactory = new FlickrServiceFactory();	
			}			
			return _serviceFactory;
		}
		
		public function FlickrServiceFactory() 
		{
		}
		
		/**
		 * Troop Service Proxy
		 */
		public static function getPhotoService(resp_:IResponder, flickr_api_key:String, flickrToken:XML, flickrSecret:String):IPhotoService
		{
			
			if (_isUseRemoteServices && _useRest) 
			{
				if (_photoServiceProxy == null) 
				{
					_photoServiceProxy = new HTTPService();
					HTTPService(_photoServiceProxy).showBusyCursor = true;				
				}
				_photoServiceProxy.addEventListener(ResultEvent.RESULT, resp_.result);
				_photoServiceProxy.addEventListener(FaultEvent.FAULT, resp_.fault);
				return new PhotoServiceRest(resp_, _photoServiceProxy, flickr_api_key, flickrToken, flickrSecret );
			} else {
				return new PhotoService();//(resp_);
			}
		}
		

	}
}