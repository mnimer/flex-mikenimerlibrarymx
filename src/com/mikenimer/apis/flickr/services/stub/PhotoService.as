package com.mikenimer.apis.flickr.services.stub
{
	import mx.rpc.AsyncToken;
	import com.mikenimer.apis.flickr.services.IPhotoService;

	public class PhotoService implements IPhotoService
	{
		public function getWithGeoData(
			min_upload_date:String=null
			,max_upload_date:String=null
			,min_taken_date:String=null
			,max_taken_date:String=null
			,privacy_filter:Number=4
			,sort:String="date-taken-asc"
			,extras:String="date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags"
			,per_page:Number=100
			,page:Number=1):AsyncToken
		{
			var args:Object = {};
				args.min_upload_date = min_upload_date;
				args.max_upload_date = max_upload_date;
				args.min_taken_date = min_taken_date;
				args.max_taken_date = max_taken_date;
				args.privacy_filter = privacy_filter;
				args.sort = sort;
				args.extras = extras;
				args.per_page = per_page;
				args.page = page;
			
			
			return null;
		}
				
	}
}