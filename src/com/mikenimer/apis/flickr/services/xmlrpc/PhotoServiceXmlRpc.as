package com.mikenimer.apis.flickr.services.xmlrpc
{
	import mx.rpc.AsyncToken;
	import com.mikenimer.apis.flickr.services.IPhotoService;
	import mx.rpc.http.HTTPService;
	import mx.rpc.IResponder;
	import mx.controls.Alert;
	import flash.net.URLRequest;

	public class PhotoServiceXmlRpc implements IPhotoService
	{
		private const xmlRestUrl:String = "http://api.flickr.com/services/rest/";
		private var apikey:String;
		private var service:HTTPService;
		
		public function PhotoServiceXmlRpc(resp:IResponder, photoServiceProxy:HTTPService, flickr_api_key:String)
		{
			this.apikey = flickr_api_key;
			this.service = photoServiceProxy;
		}
		
		
		public function getWithGeoData(
			min_upload_date:String=null
			,max_upload_date:String=null
			,min_taken_date:String=null
			,max_taken_date:String=null
			,privacy_filter:Number=4
			,sort:String="date-taken-asc"
			,extras:String="date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags"
			,per_page:Number=100
			,page:Number=1):AsyncToken
		{
			var args:Object = {};
				args.api_key = apikey;
				args.min_upload_date = min_upload_date;
				args.max_upload_date = max_upload_date;
				args.min_taken_date = min_taken_date;
				args.max_taken_date = max_taken_date;
				args.privacy_filter = privacy_filter;
				args.sort = sort;
				args.extras = extras;
				args.per_page = per_page;
				args.page = page;
			
			var xml:XML = getRequestXml("getWithGeoData", args);
			
			service.url = "http://api.flickr.com/services/xmlrpc/";
			service.method = "POST";
			service.resultFormat = "e4x";
			return service.send(xml);
		}
		
		
		
		private function getRequestXml(method:String, args:Object):XML
		{
			// default Flickr XML-RPC format for a request.
			var xml:XML = 
				<methodCall>
					<methodName>{method}</methodName>
					<params>
						<param>
							<value>
								<struct/>
							</value>
						</param>
					</params>
				</methodCall>
				
			// add the items in the args Object as members
			for( var key:Object in args )
			{
				if( args[key] != null )
				{
					var xmlChild:XML = 
						<member>
							<name>{key}</name>
							<value><string>{args[key]}</string></value>
						</member>
					xml.params.param.value.struct.appendChild(xmlChild);
				}
			}

			return xml;
		}

		
	}
}