package com.mikenimer.apis.flickr.services
{
	import mx.rpc.AsyncToken;

	
	public interface IPhotoService
	{
		function getWithGeoData(
			min_upload_date:String=null
			,max_upload_date:String=null
			,min_taken_date:String=null
			,max_taken_date:String=null
			,privacy_filter:Number=1
			,sort:String="date-taken-asc"
			,extras:String="date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags"
			,per_page:Number=500
			,page:Number=1):AsyncToken;
					
	}
}