package com.mikenimer.apis.flickr.services.rest
{
	import mx.rpc.AsyncToken;
	import com.mikenimer.apis.flickr.services.IPhotoService;
	import mx.rpc.http.HTTPService;
	import mx.rpc.IResponder;
	import mx.controls.Alert;
	import flash.net.URLRequest;
	import com.adobe.crypto.MD5;

	public class PhotoServiceRest implements IPhotoService
	{
		private const xmlRestUrl:String = "http://api.flickr.com/services/rest/";
		private var apikey:String;
		private var apiToken:XML;
		private var apiSecret:String;
		private var service:HTTPService;
		

		public function PhotoServiceRest(resp:IResponder, photoServiceProxy:HTTPService, flickr_api_key:String, flickrToken:XML, secret:String)
		{
			this.apikey = flickr_api_key;
			this.apiToken = flickrToken;
			this.service = photoServiceProxy;
			this.apiSecret = secret;
		}
		
		
		public function getWithGeoData(
			min_upload_date:String=null
			,max_upload_date:String=null
			,min_taken_date:String=null
			,max_taken_date:String=null
			,privacy_filter:Number=4
			,sort:String="date-taken-asc"
			,extras:String="date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags"
			,per_page:Number=500
			,page:Number=1):AsyncToken
		{
			var args:Object = {};
				args.api_key = apikey;
				args.auth_token=apiToken..token;
				args.extras = extras;
				args.max_taken_date = max_taken_date;
				args.max_upload_date = max_upload_date;
				args.method="flickr.photos.getWithGeoData";
				args.min_taken_date = min_taken_date;
				args.min_upload_date = min_upload_date;
				args.page = page;
				args.per_page = per_page;
				args.privacy_filter = privacy_filter;
				args.sort = sort;
			
			var sig:String = getSigKey(this.apiSecret, args);
			args.api_sig = sig;
							
			service.url = "http://api.flickr.com/services/rest/";
			service.method = "get";
			service.resultFormat = "e4x";
			return service.send(args);
		}


		private function getSigKey(secret:String, args:Object):String
		{
			var str:String = secret;
			var keys:Array = [];
			for( var key:Object in args )
			{
				keys.push(key);
			}
			
			keys.sort();
			
			for( var i:int=0; i < keys.length; i++ )
			{
				if( args[keys[i]] != null )
				{
					str += keys[i] +args[keys[i]];
				}
			}
			
			return MD5.hash(str);
		}
	}
}