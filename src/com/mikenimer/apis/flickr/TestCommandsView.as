package com.mikenimer.apis.flickr
{
	import mx.core.Application;
	import flash.events.Event;
	import mx.core.ClassFactory;
	import flash.utils.describeType;
	import mx.utils.ObjectUtil;

	public class TestCommandsView extends Application
	{
		public function TestCommandsView()
		{
			super();
		}
		
		/**
		 * take the selected command, parse out the arguments to display.
		 **/
		public function doTestCommand(event:Event):void
		{
			var cls:Class = event.currentTarget.selectedItem.command;
			var description:XML = describeType(cls);

			var info:* = ObjectUtil.getClassInfo(cls);
			//trace(description.toXMLString());
			//trace("******************");
			var name:String = description.@name;
			//trace( description.method.(@declaredBy==name).toXMLString() );
		}
		
	}
}