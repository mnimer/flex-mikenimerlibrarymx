package mx.rpc
{
	
	import mx.core.mx_internal;
	import mx.messaging.messages.AsyncMessage;
	import mx.messaging.messages.IMessage;
	use namespace mx_internal;

	dynamic public class XMLAsyncToken extends AsyncToken
	{
	    public function applyGenericFault(result:Object):void
	    {
			if (responders != null)
	    	{
	    	    for (var i:uint = 0; i < responders.length; i++)
	            {
	                var responder:IResponder = responders[i];
	                if (responder != null)
	                {
	                    responder.fault(result);
	                }
	            }
	    	}
	    }
	
	    /**
	     * @private
	     */
	    public function applyGenericResult(info:Object):void
	    {
	        setResult(info);
	
	    	if (responders != null)
	    	{
	    	    for (var i:uint = 0; i < responders.length; i++)
	            {
	                var responder:IResponder = responders[i];
	                if (responder != null)
	                {
	                    responder.result(info);
	                }
	            }
	    	}
	    }

		public function XMLAsyncToken(message:XML)
		{
			var msg:IMessage = new AsyncMessage(message);
			super(msg);
		}
		
	}
}